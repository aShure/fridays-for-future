# Handzettel Solidarität

Die Aktion-Reaktion-Kette ist sehr lang. Rückwärts gesehen, ist der Schulstreik eine Reaktion auf den Klimawandel, welcher eine Reaktion auf die Ausbeutung der Natur ist. Vorwärts gesehen, werden auf den Schulstreik Reaktion folgen, eine ist bereits bekannt: 
> Im Februar 2019 erklärte die nordrhein-westfälische Schulministerin Yvonne Gebauer, sie wolle hart gegen Schulstreikende vorgehen. Es reiche nicht aus, wenn unentschuldigt versäumte Stunden dokumentiert und auf dem nächsten Zeugnis deren Zahl veröffentlicht werde [^wiki-fff][^gebauer-t-online].

## Das kannst du machen (lassen)

Um die Schüler/Aktivisten jetzt zu verteidigen kannst du folgendes machen oder machen lassen:

* Schulleitungen anrufen und enthusiastisch von den Schülern berichten, die sich so großartig gesellschaftspolitisch engagieren. Eine Liste aller Schulen stellt uns freundlicherweise die Stadt Siegen zur Verfügung[^liste-schulen]. Also, anrufen, freundlich Unterrichtsbefreiung fordern und wiederholen.

* Unsere gestrenge Schulministerin Yvonne Gebauer ist unser aller Schulministerin [^wiki-gebauer][^landtag-nrw-gebauer]. Die Schulen bilden Kinder und Jugendliche auch zu mündigen Bürgern aus, wir sollten der Ministerin zu ihrem Erfolg gratulieren. Und wenn sie streng gegen unsere Schüler/Aktivisten vorgeht, dann wollen wir streng gegen ihre Partei vorgehen: Nie wieder FDP wählen. Wir dürfen im Telefonat mit ihr oder ihrem Büro auch erwähnen, dass die Bundeskanzlerin das Engagement der Schüler für Klimapolitik ausdrücklich gut findet [^twitter-gebauer].
    * Telefon: (0211) 884-4429
    * E-Mail: yvonne.gebauer@landtag.nrw.de 

* Unser gestrenger Landesvater Armin Laschet [^wiki-laschet], [^landtag-nrw-laschet] bewertet die Streiks als Verstoß gegen die Schulpflicht und will die Rechtswidrikeit mit angemessenen Maßnahmen klar machen [^wdr-laschet]. Eine angemessene Maßnahme von uns, ist ihn anzurufen und freundlich klar zu machen, dass das soziale Engagement und die Dringlichkeit höher zu bewerten ist, als die Schulpflicht.
    * Telefon: (0211) 884-4470
    * E-Mail: armin.laschet@landtag.nrw.de 

 ^wiki-fff: https://de.wikipedia.org/wiki/Fridays_For_Future
 
 ^gebauer-t-online: https://www.t-online.de/nachrichten/id_85261858/ministerin-will-schulpflicht-durchsetzen.html
 
 ^liste-schulen: https://www.siegen.de/leben-in-siegen/bildung/schulen/
 
 ^wiki-gebauer: https://de.wikipedia.org/wiki/Yvonne_Gebauer
 
 ^landtag-nrw-gebauer: https://www.landtag.nrw.de/portal/WWW/Webmaster/GB_I/I.1/Abgeordnete/abgeordnetendetail.jsp?k=01628
 
 ^twitter-gebauer: https://twitter.com/RegSprecher/status/1096757385836810240
 
 ^wiki-laschet: https://de.wikipedia.org/wiki/Armin_Laschet
 
 ^landtag-nrw-laschet: https://www.landtag.nrw.de/portal/WWW/Webmaster/GB_I/I.1/Abgeordnete/abgeordnetendetail.jsp?k=01569
 
 ^wdr-laschet: https://www1.wdr.de/nachrichten/landespolitik/friday-for-future-laschet-kritik-100.html

Weitere Ideen per E-Mail an tau@chaos-siegen.de.